package com.zeytol.conex_sqlserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConexSqlserverApplication {

    public static void main(String[] args) {
        SpringApplication.run(ConexSqlserverApplication.class, args);
    }

}
