package com.zeytol.conex_sqlserver.repositories;

import com.zeytol.conex_sqlserver.models.Persona;
import org.springframework.data.repository.CrudRepository;

public interface PersonaRepositoryImpl extends CrudRepository<Persona, Long> {
}
