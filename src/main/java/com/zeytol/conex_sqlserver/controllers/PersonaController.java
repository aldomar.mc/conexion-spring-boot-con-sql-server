package com.zeytol.conex_sqlserver.controllers;

import com.zeytol.conex_sqlserver.models.Persona;
import com.zeytol.conex_sqlserver.services.PersonaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class PersonaController {
    @Autowired
    private PersonaService personaService;

    //Listado de personas
    @GetMapping
    public List<Persona> listAll() {
        List<Persona> getAll = null;
        try {
            getAll = personaService.findAll();
        } catch (Exception e) {
            System.out.println("Error: " + e);
        }
        return getAll;
    }
}
