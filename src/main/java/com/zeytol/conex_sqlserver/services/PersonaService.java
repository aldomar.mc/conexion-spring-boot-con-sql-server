package com.zeytol.conex_sqlserver.services;

import com.zeytol.conex_sqlserver.models.Persona;

import java.util.List;

public interface PersonaService {
    public List<Persona> findAll();
}
