package com.zeytol.conex_sqlserver.services;

import com.zeytol.conex_sqlserver.models.Persona;
import com.zeytol.conex_sqlserver.repositories.PersonaRepositoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonaServiceImpl implements PersonaService {
    @Autowired
    private PersonaRepositoryImpl personaRepository;

    @Override
    public List<Persona> findAll() {
        return (List<Persona>) personaRepository.findAll();
    }
}
